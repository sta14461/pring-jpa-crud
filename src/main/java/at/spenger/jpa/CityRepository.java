package at.spenger.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;	

public interface CityRepository extends CrudRepository<City, Integer>
{
    List<City> findAll();    
}
